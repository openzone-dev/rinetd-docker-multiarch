# Rinetd Docker MultiArch Image

General purpose TCP/UDP redirection service running in Docker. This image is mainly based on Alpine, and is built using layers to reduce the final image size. Rinetd itself is compiled at build time, thus enabling multiarch support.

## Links

- Repository: https://gitlab.com/openzone-dev/rinetd-docker-multiarch
- Docker Hub: https://hub.docker.com/r/openzonedev/rinetd
- Gitlab Container Registry: https://gitlab.com/openzone-dev/rinetd-docker-multiarch/container_registry

## ENV Vars

This image doesn't make use of environment variables, and uses configuration files instead. Support for env vars may be added in the future.

## Volumes

Configuration files volume:

```
/etc/rinetd
```

Logs volume:

```
/var/log/rinetd
```

The configuration file itself should be placed in /etc/rinetd/rinetd.conf. For an example configuration file, check the Gitlab repo `rinetd/config/rinetd.conf` configuration file.

## Usage

A working Docker Compose file example.

```
  rinetd:
    container_name: rinetd
    image: openzonedev/rinetd:0.73-alpine3.16
    ports:
      - '80:80'
      - '443:443'
      - '27015:27015/udp'
    volumes:
      - ./rinetd/config:/etc/rinetd
      - ./rinetd/logs:/var/log/rinetd
```

Note that for Rinetd to redirect traffic, the container must have access to the traffic on those ports. Therefore, make sure to open the ports to the container in the docker-compose.yml file. In this example, the ports 80, 443, and 27015 UDP are open to the container.

A configuration file must be mounted to Rinetd. For further instructions on how to write this file, check `manpage.html`, which contains the man page of the built Rinetd version.

Note that, you may find this image in both Docker Hub (openzonedev/rinetd) and Gitlab Container Registry (registry.gitlab.com/openzone-dev/rinetd-docker-multiarch/rinetd), both Public.

## Build

Pre-requisites:
- Docker
- Docker BuildX extension with an experimental builder
- Make

You must create a BuildX builder with enabled experimental build features. You must append the architecture builders you want to build for, to the original builder. These builds are done by our team, and are going to be automated in the future.

To build and push the image, run:

```
make build
```

## Collaborate

Further improvement to the image are more than welcome. These improvements must be in branches named after the push tag. 

Ex: 0.73-alpine3.16 is a Rinetd 0.73, Alpine 3.16 image. For support (non-breaking) changes, it's allowed to push directly to this branch. For breaking changes, you must make a v2 branch, like so: 0.73-alpine3.16v2.

Ex: 0.73-ubuntuslim-22.04 is a Rinetd 0.73 image, Ubuntu Slim 22.04 image.